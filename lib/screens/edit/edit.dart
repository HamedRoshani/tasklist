import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:tsklist/data/data.dart';
import 'package:tsklist/data/repo/repository.dart';

import 'package:tsklist/main.dart';

class EditetaskScreen extends StatefulWidget {
  final TaskEntity task;

  EditetaskScreen({Key? key, required this.task}) : super(key: key);

  @override
  State<EditetaskScreen> createState() => _EditetaskScreenState();
}

class _EditetaskScreenState extends State<EditetaskScreen> {
  late TextEditingController _controller =
      TextEditingController(text: widget.task.name);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Scaffold(
      backgroundColor: themeData.colorScheme.surface,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            widget.task.name = _controller.text;
            widget.task.priority = widget.task.priority;
            final repository =
                Provider.of<Repository<TaskEntity>>(context, listen: false);
            repository.createOrUpdata(widget.task);

            Navigator.of(context).pop();
          },
          label: Row(
            children: [
              Text('Save Changes'),
              Icon(
                CupertinoIcons.check_mark,
                size: 18,
              ),
            ],
          )),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: themeData.colorScheme.surface,
        foregroundColor: themeData.colorScheme.onSurface,
        title: Text('Edite Task'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Flex(
              direction: Axis.horizontal,
              children: [
                Flexible(
                  flex: 1,
                  child: PriorityCheckBox(
                    ontap: () {
                      setState(() {
                        widget.task.priority = Priorityy.high;
                      });
                    },
                    label: 'High',
                    color: highpriorityColor,
                    isSelected: widget.task.priority == Priorityy.high,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Flexible(
                  flex: 1,
                  child: PriorityCheckBox(
                    ontap: () {
                      setState(() {
                        widget.task.priority = Priorityy.normal;
                      });
                    },
                    label: 'Normal',
                    color: normalPriorityColor,
                    isSelected: widget.task.priority == Priorityy.normal,
                  ),
                ),
                SizedBox(
                  width: 8,
                ),
                Flexible(
                  flex: 1,
                  child: PriorityCheckBox(
                    ontap: () {
                      setState(() {
                        widget.task.priority = Priorityy.low;
                      });
                    },
                    label: 'Low',
                    color: lowpriorityColor,
                    isSelected: widget.task.priority == Priorityy.low,
                  ),
                ),
              ],
            ),
            TextField(
              controller: _controller,
              decoration: InputDecoration(
                label: Text('Add Task For Today ..'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PriorityCheckBox extends StatelessWidget {
  final String label;
  final Color color;
  final bool isSelected;
  final GestureTapCallback ontap;

  const PriorityCheckBox(
      {Key? key,
      required this.label,
      required this.color,
      required this.isSelected,
      required this.ontap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(
            width: 2,
            color: secondarytextcolor.withOpacity(0.2),
          ),
        ),
        child: Stack(
          children: [
            Center(
              child: Text(label),
            ),
            Positioned(
                right: 7,
                top: 0,
                bottom: 0,
                child: Center(
                    child: PrioritycheckBoxShape(
                        value: isSelected, color: color))),
          ],
        ),
      ),
    );
  }
}

class PrioritycheckBoxShape extends StatelessWidget {
  final bool value;
  final Color color;
  const PrioritycheckBoxShape(
      {Key? key, required this.value, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 16,
      height: 16,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(12), color: color),
      child: value
          ? Icon(
              CupertinoIcons.check_mark,
              color: Colors.white,
              size: 12,
            )
          : null,
    );
  }
}
