import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tsklist/main.dart';
import 'package:provider/provider.dart';
import 'package:tsklist/data/data.dart';
import 'package:tsklist/data/repo/repository.dart';
import 'package:tsklist/main.dart';
import 'package:tsklist/screens/edit/edit.dart';
import 'package:tsklist/screens/home/bloc/tasklist_bloc.dart';

import 'package:tsklist/widgets.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => EditetaskScreen(
              task: TaskEntity(),
            ),
          ));
        },
        label: Row(
          children: const [
            Text('Add New Task'),
            SizedBox(
              width: 4,
            ),
            Icon(CupertinoIcons.plus_circle)
          ],
        ),
      ),
      body: BlocProvider(
        create: (context) {
          return TaskListBloc(context.read<Repository<TaskEntity>>());
        },
        child: SafeArea(
          child: Column(
            children: [
              Container(
                height: 110,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      themeData.colorScheme.primary,
                      themeData.colorScheme.primaryVariant,
                    ],
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'To Do List',
                            style: themeData.textTheme.headline6!.copyWith(
                                color: themeData.colorScheme.onPrimary),
                          ),
                          Icon(
                            CupertinoIcons.share,
                            color: themeData.colorScheme.onPrimary,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(19),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                blurRadius: 20,
                              )
                            ]),
                        height: 38,
                        width: double.infinity,
                        child: Builder(
                          builder: (context) => TextField(
                            onChanged: (value) {
                              context
                                  .read<TaskListBloc>()
                                  .add(TaskListSearch(value));
                            },
                            controller: controller,
                            decoration: const InputDecoration(
                              prefixIcon: Icon(CupertinoIcons.search),
                              label: Text('Search Tasks...'),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(child: Consumer<Repository<TaskEntity>>(
                builder: ((context, value, child) {
                  context.read<TaskListBloc>().add(TaskListStarted());
                  return BlocBuilder<TaskListBloc, TaskListState>(
                    builder: (context, state) {
                      if (state is TaskListSucccess) {
                        return TaskList(
                            items: state.items, themeData: themeData);
                      } else if (state is TaskListEmpty) {
                        return const Emptystate();
                      } else if (state is TaskListLoading ||
                          state is TaskListInitial) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is TaskListError) {
                        return Center(
                          child: Text(state.errorMessage),
                        );
                      } else {
                        throw Exception('State is not valid ');
                      }
                    },
                  );
                }),
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({
    Key? key,
    required this.items,
    required this.themeData,
  }) : super(key: key);

  final items;
  final ThemeData themeData;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 100),
      itemCount: items.length + 1,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'To Day',
                    style: themeData.textTheme.headline6!
                        .apply(fontSizeFactor: 0.8),
                  ),
                  Container(
                    width: 60,
                    height: 3,
                    margin: const EdgeInsets.only(top: 5),
                    decoration: BoxDecoration(
                      color: themeData.colorScheme.primary,
                      borderRadius: BorderRadius.circular(1.5),
                    ),
                  )
                ],
              ),
              MaterialButton(
                color: const Color(0xffEAEFF5),
                textColor: secondarytextcolor,
                elevation: 0,
                onPressed: () {
                  context.read<TaskListBloc>().add(TaskListDeleteAll());
                },
                child: Row(
                  children: const [
                    Text('Delete All'),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      CupertinoIcons.delete,
                      size: 18,
                    ),
                  ],
                ),
              )
            ],
          );
        } else {
          final TaskEntity task = items[index - 1];
          return Taskitem(task: task);
        }
      },
    );
  }
}

class Taskitem extends StatefulWidget {
  static const double height = 84;
  const Taskitem({
    Key? key,
    required this.task,
  }) : super(key: key);

  final TaskEntity task;

  @override
  State<Taskitem> createState() => _TaskitemState();
}

class _TaskitemState extends State<Taskitem> {
  @override
  Widget build(BuildContext context) {
    final Color prioritycolor;
    switch (widget.task.priority) {
      case Priorityy.low:
        prioritycolor = lowpriorityColor;
        break;
      case Priorityy.normal:
        prioritycolor = normalPriorityColor;

        break;
      case Priorityy.high:
        prioritycolor = highpriorityColor;

        break;
    }
    ThemeData themeData = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Material(
        child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => EditetaskScreen(task: widget.task),
              ),
            );
          },
          onLongPress: () {
            widget.task.delete();
          },
          child: Container(
            //margin: EdgeInsets.only(bottom: 10),
            padding: EdgeInsets.only(left: 16),
            height: Taskitem.height,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.transparent,
            ),
            child: Row(
              children: [
                Mycheckbox(
                  value: widget.task.isCompleted,
                  ontap: () {
                    setState(() {
                      widget.task.isCompleted = !widget.task.isCompleted;
                    });
                  },
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Text(
                    widget.task.name,
                    style: TextStyle(
                        overflow: TextOverflow.ellipsis,
                        fontSize: 24,
                        decoration: widget.task.isCompleted
                            ? TextDecoration.lineThrough
                            : null),
                  ),
                ),
                Container(
                  width: 5,
                  height: Taskitem.height,
                  decoration: BoxDecoration(
                    color: prioritycolor,
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(8),
                      bottomRight: Radius.circular(8),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        color: themeData.colorScheme.surface,
      ),
    );
  }
}
