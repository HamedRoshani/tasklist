import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:tsklist/data/repo/repository.dart';
import 'package:tsklist/widgets.dart';

import '../../../data/data.dart';

part 'tasklist_event.dart';
part 'tasklist_state.dart';

class TaskListBloc extends Bloc<TaskListEvent, TaskListState> {
  final Repository<TaskEntity> repository;
  TaskListBloc(this.repository) : super(TaskListInitial()) {
    on<TaskListEvent>((event, emit) async {
      if (event is TaskListStarted || event is TaskListSearch) {
        final String searchTerm;
        emit(TaskListLoading());
        await Future.delayed(Duration(seconds: 1));
        if (event is TaskListSearch) {
          searchTerm = event.searchTerm;
        } else {
          searchTerm = '';
        }
        try {
          final List<TaskEntity> items =
              await repository.getAll(searchkeyword: searchTerm);
          if (items.isNotEmpty) {
            emit(TaskListSucccess(items));
          } else {
            emit(TaskListEmpty());
          }
        } catch (e) {
          emit(TaskListError("خظای نامشخص"));
        }
      } else if (event is TaskListDeleteAll) {
        await repository.deleteAll();
        emit(TaskListEmpty());
      }

      // TODO: implement event handler
    });
  }
}
