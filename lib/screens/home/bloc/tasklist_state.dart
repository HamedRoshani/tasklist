part of 'tasklist_bloc.dart';

@immutable
abstract class TaskListState {}

class TaskListInitial extends TaskListState {}

class TaskListLoading extends TaskListState {}

class TaskListSucccess extends TaskListState {
  final List<TaskEntity> items;

  TaskListSucccess(this.items);
}

class TaskListEmpty extends TaskListState {
  
}

class TaskListError extends TaskListState {
  final String errorMessage;

  TaskListError(this.errorMessage);
}
