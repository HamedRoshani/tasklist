import 'package:hive_flutter/adapters.dart';
import 'package:tsklist/data/data.dart';
import 'package:tsklist/data/sorce/sorce.dart';

class HiveTaskDataSorce implements Datasorce<TaskEntity> {
  final Box<TaskEntity> box;

  HiveTaskDataSorce(this.box);
  @override
  Future<TaskEntity> createOrUpdata(TaskEntity data) async {
    if (data.isInBox) {
      data.save();
    } else {
      data.id = await box.add(data);
    }
    return data;
  }

  @override
  Future<void> delete(TaskEntity data) async {
    return data.delete();
  }

  @override
  Future<void> deleteAll() {
    return box.clear();
  }

  @override
  Future<void> deleteById(id) {
    return box.delete(id);
  }

  @override
  Future<TaskEntity> findById(id) async {
    return box.values.firstWhere((element) => element.id == id);
  }

  @override
  Future<List<TaskEntity>> getAll({String searchkeyword = ''}) async {
    if (box.values.isNotEmpty) {
      return box.values
          .where((element) => element.name.contains(searchkeyword))
          .toList();
    } else {
      return box.values.toList();
    }
  }
}
