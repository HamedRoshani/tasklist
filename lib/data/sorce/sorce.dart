abstract class Datasorce<T> {
  Future<List<T>> getAll({String searchkeyword});

  Future<T> findById(dynamic id);

  Future<void> deleteAll();

  Future<void> delete(T data);
  Future<void> deleteById(dynamic id);

  Future<T> createOrUpdata(T data);
}
