// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TaskAdapter extends TypeAdapter<TaskEntity> {
  @override
  final int typeId = 0;

  @override
  TaskEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TaskEntity()
      ..name = fields[0] as String
      ..isCompleted = fields[1] as bool
      ..priority = fields[2] as Priorityy;
  }

  @override
  void write(BinaryWriter writer, TaskEntity obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.isCompleted)
      ..writeByte(2)
      ..write(obj.priority);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TaskAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PriorityAdapter extends TypeAdapter<Priorityy> {
  @override
  final int typeId = 1;

  @override
  Priorityy read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Priorityy.low;
      case 1:
        return Priorityy.normal;
      case 2:
        return Priorityy.high;
      default:
        return Priorityy.low;
    }
  }

  @override
  void write(BinaryWriter writer, Priorityy obj) {
    switch (obj) {
      case Priorityy.low:
        writer.writeByte(0);
        break;
      case Priorityy.normal:
        writer.writeByte(1);
        break;
      case Priorityy.high:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PriorityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
