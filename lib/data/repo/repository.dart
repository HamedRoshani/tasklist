import 'package:flutter/cupertino.dart';
import 'package:tsklist/data/sorce/sorce.dart';

class Repository<T> extends ChangeNotifier implements Datasorce {
  final Datasorce<T> localDatasorce;

  Repository(this.localDatasorce);
  @override
  Future createOrUpdata(data) async {
    final T result = await localDatasorce.createOrUpdata(data);
    notifyListeners();
    return result;
  }

  @override
  Future<void> delete(data) async {
    localDatasorce.delete(data);
    notifyListeners();
  }

  @override
  Future<void> deleteAll() async {
    await localDatasorce.deleteAll();
    notifyListeners();
  }

  @override
  Future<void> deleteById(id) async {
    localDatasorce.deleteById(id);
    notifyListeners();
  }

  @override
  Future findById(id) {
    return localDatasorce.findById(id);
  }

  @override
  Future<List<T>> getAll({String searchkeyword = ''}) {
    return localDatasorce.getAll(searchkeyword: searchkeyword);
  }
}
