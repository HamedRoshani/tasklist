import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tsklist/main.dart';

class Emptystate extends StatelessWidget {
  const Emptystate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          'assets/empty_state.svg',
          width: 120,
        ),
        SizedBox(
          height: 12,
        ),
        Text('Your TAsk List Is Empty')
      ],
    );
  }
}

class Mycheckbox extends StatelessWidget {
  final bool value;
  final GestureTapCallback ontap;

  const Mycheckbox({Key? key, required this.value, required this.ontap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        width: 24,
        height: 24,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border:
              value ? null : Border.all(color: secondarytextcolor, width: 2),
          color: value ? primarycolor : null,
        ),
        child: value
            ? Icon(
                CupertinoIcons.check_mark,
                color: Colors.white,
                size: 16,
              )
            : null,
      ),
    );
  }
}
