import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

import 'package:tsklist/data/data.dart';
import 'package:tsklist/data/repo/repository.dart';
import 'package:tsklist/data/sorce/hive_task_sorce.dart';

import 'package:tsklist/screens/home/home.dart';

const taskBoxName = 'tasks';
void main() async {
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: primaryvariantcolor));
  await Hive.initFlutter();
  Hive.registerAdapter(TaskAdapter());
  Hive.registerAdapter(PriorityAdapter());
  await Hive.openBox<TaskEntity>(taskBoxName);

  runApp(ChangeNotifierProvider<Repository<TaskEntity>>(
      create: (context) {
        return Repository<TaskEntity>(
          HiveTaskDataSorce(
            Hive.box(taskBoxName),
          ),
        );
      },
      child: const MyApp()));
}

const primarycolor = Color(0xff794CFF);
const primaryvariantcolor = Color(0xff5C0AFF);
const secondarytextcolor = Color(0xffAFBED0);
const normalPriorityColor = Color(0xffF09819);
const lowpriorityColor = Color(0xff3BE1F1);
const highpriorityColor = primarycolor;

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final primarytextcolor = Color(0xff1D2830);

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: GoogleFonts.poppinsTextTheme(
            TextTheme(headline6: TextStyle(fontWeight: FontWeight.bold))),
        inputDecorationTheme: InputDecorationTheme(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            border: InputBorder.none,
            labelStyle: TextStyle(
              color: secondarytextcolor,
            ),
            prefixIconColor: secondarytextcolor),
        colorScheme: ColorScheme.light(
          primary: primarycolor,
          primaryVariant: primaryvariantcolor,
          background: Color(0xffF3F5F8),
          onSurface: primarytextcolor,
          onBackground: primarytextcolor,
          secondary: primarycolor,
          onSecondary: Colors.white,
        ),
      ),
      home: HomeScreen(),
    );
  }
}
